# Predicting With Us - Backend


## How to run at Local:
1. Install GHCi
2. Clone this repository
2. Run `cabal install`
2. Run `cabal run`

## Or use docker-compose:
`$ docker-compose up --build`

## Live version
http://35.197.134.5/

## Api Documentation

### 1. Get formula from input data

- Endpoint: `/lsp`
- Method: `POST`
- DataType: `application/json`

- Input Body Example:
```json
{
	"values": [0, 1, 4, 9, 16]
}
```

- Response Body Example:
```json
{
	"values": [1, 2, 3]
}
```

- Explanation:

	- For Input Body example above, variable 'values' is an array of value per year, 0 means the value in the first year, and so on.
	- For Response Body example above, variable 'values' is an array of constant in formula used, for example 1, 2, 3 mean 3x²+2x+1