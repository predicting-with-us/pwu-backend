{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import GHC.Generics
import Web.Scotty
import Network.HTTP.Types
import Data.Aeson (FromJSON, ToJSON)
import Matrix (f, pow)
import Network.Wai.Middleware.Cors

data JsonFormat = JsonFormat { values :: [Float] } deriving (Show, Generic)
instance ToJSON JsonFormat
instance FromJSON JsonFormat

main = scotty 3001 $ do
  middleware simpleCors
  post "/lsp" $ do
    let degree = 2
    rawJson <- jsonData :: ActionM JsonFormat 
    let dataInput = values rawJson
    let b = [[row] | row <- dataInput]
    let a = [[(fromIntegral i) `pow` j | j <- [0..degree]] | i <- [0..(length dataInput)-1]]
    let formula = map (\row -> row !! 0) (f a b)
    let result = JsonFormat { values = formula }
    json result
  
