module Matrix (multiply, transpose, remove_row, remove_col, minor, mul_const, determinant, inverse, adj, pow, f) where

transpose               :: [[Float]] -> [[Float]]
transpose []             = []
transpose ([]   : xss)   = transpose xss
transpose ((x:xs) : xss) = (x : [h | (h:_) <- xss]) : transpose (xs : [ t | (_:t) <- xss])

zipping f [] [] = [] 
zipping f _ [] = error "Length of matrix1 and matrix2 are not possible to multiply"  -- if ordo matrix not match ex : M x N and C x N
zipping f [] _ = error "Length of matrix1 and matrix2 are not possible to multiply" -- if ordo matrix not match ex : N x M and C x N
zipping f (x:xs) (y:ys) = x `f` y : (zipping f xs ys)

multiply :: [[Float]] -> [[Float]] -> [[Float]]
multiply xs ys = [ [sum (zipping (*) elementy elementx) | elementy <- (transpose ys) ] | elementx <- xs] -- for row in matrix1 multiply it with row in transpose matrix2

mul_const ::  Float -> [[Float]] -> [[Float]]
mul_const c = map (\row -> map (*c) row)

remove_row :: [[Float]] -> Int -> [[Float]]
remove_row (x:xs) 0 = xs
remove_row (x:xs) n = x:(remove_row xs (n - 1))

remove_col :: [[Float]] -> Int -> [[Float]]
remove_col x = transpose.(remove_row (transpose x))

minor :: [[Float]] -> Int -> Int -> [[Float]]
minor m = remove_col.(remove_row m)

pow :: Float -> Int -> Float
pow a 0 = 1
pow a b = a * (pow a (b - 1))

determinant :: [[Float]] -> Float
determinant ([x]:_) = x
determinant m = sum [((-1) `pow` i) * (m !! 0 !! i) * (determinant (minor m 0 i)) | i <- [0..((length m) - 1)]]

adj :: [[Float]] -> [[Float]]
adj m = transpose [[(determinant (minor m i j)) * ((-1) `pow` (i+j)) | j <- [0..((length m) - 1)]] | i <- [0..((length m) - 1)]]

inverse :: [[Float]] -> [[Float]]
inverse xss = [[x / determinant (xss) | x <- xs] | xs <- adj xss]

f :: [[Float]] -> [[Float]] -> [[Float]]
f a b = ((inverse ((transpose a) `multiply` a)) `multiply` (transpose a)) `multiply` b