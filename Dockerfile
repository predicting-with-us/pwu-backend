FROM haskell:8.0.2

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY pwu.cabal /usr/src/app/

RUN cabal update

RUN cabal install --dependencies-only -j4

COPY . /usr/src/app

EXPOSE 3001

CMD [ "cabal", "run" ]